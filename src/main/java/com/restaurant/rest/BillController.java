package com.restaurant.rest;


import com.restaurant.dto.BillDTO;
import com.restaurant.dto.BillItemDTO;
import com.restaurant.service.BillItemService;
import com.restaurant.service.BillService;
import com.restaurant.utils.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

/**
 * Rest Controller for bill
 */
@RestController
@RequestMapping(Constant.billUrl)
public class BillController {
    private final String API_BILL_ITEMS_URL = Constant.billItemsUrl;
    private static final String ID = "id";

    @Autowired
    private BillService billService;
    @Autowired
    private BillItemService billItemService;

    /**
     * Find all bil in the database
     * with sortBy or page (5 objects per page)
     */
    @GetMapping
    @ApiOperation(value = "Get list of bills with options: sortBy(ASC), page - default = 0, size - default = 5.")
    public  ResponseEntity<Page<BillDTO>> getAllBills(@RequestParam Optional<String> sortBy,
                                                      @RequestParam Optional<Integer> page,
                                                      @RequestParam Optional<Integer> size) {
        return new ResponseEntity<>(billService.findAll(page, sortBy, size), HttpStatus.OK);
    }

    /**
     * Get bill by id -> return bill and http status
     */
    @GetMapping( "/{"+ID+"}")
    @ApiOperation(value = "Get bill by id, return a bill with given id and the http status code.")
    public ResponseEntity<BillDTO> getBillById(@PathVariable int id) {

        BillDTO billDTO = billService.findById(id);
        return new ResponseEntity<>(billDTO, HttpStatus.OK);
    }

    /**
     * Add bill to the database, return added bill and http status
     */
    @PostMapping
    @ApiOperation(value = "Add a bill to the database. Return the new bill and the http status code.")
    public ResponseEntity<BillDTO> createBill(@RequestBody BillDTO billDTO) {
        BillDTO resultBillDTO = billService.createOrUpdate(billDTO, 0, true);
        return new ResponseEntity<>(resultBillDTO, HttpStatus.OK);
    }

    /**
     * Create bill by list of bill items -> /bill/{id}/billitem
     * @param id
     * @param billDTO
     * @return
     */
    @PostMapping("/{id}/billitem")
    @ApiOperation(value = "Add a bill by list of bill items with path: /api/bill/{id}/billitem.")
    public ResponseEntity<BillDTO> createListBillItems(@PathVariable int id, @RequestBody BillDTO billDTO) {
        BillDTO resultBillDTO = billService.createListBillItems(billDTO, id);
        return new ResponseEntity<>(resultBillDTO, HttpStatus.OK);
    }



    /**
     * Update existing bill -> update an existing bill and return new bill with http status
     */
    @PutMapping("/{"+ID+"}")
    @ApiOperation(value = "Update a given bill by id (date).")
    public ResponseEntity<BillDTO> updateBillById(@PathVariable int id,@RequestBody BillDTO billDTO) {

        BillDTO resultBillDTO = billService.createOrUpdate(billDTO, id, false);
        return new ResponseEntity<>(resultBillDTO, HttpStatus.OK);
    }

    /**
     * Delete existing bill
     */
    @DeleteMapping("/{"+ID+"}")
    @ApiOperation(value = "Delete a bill by id")
    public ResponseEntity<Void> deleteBillById(@PathVariable int id) {

        billService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Get all menu items
     * sortBy(id, sub_total, ...)
     * page : 'size' objects / page
     */
    @GetMapping(API_BILL_ITEMS_URL)
    @ApiOperation(value = "Get all bill items with options: sortBy(ASC), page - default: 0, size - default: 5")
    public Page<BillItemDTO> getAllBillItems(@RequestParam Optional<String> sortBy,
                                             @RequestParam Optional<Integer> page,
                                             @RequestParam Optional<Integer> size) {
        return billItemService.findAll(page, sortBy, size);
    }

    /**
     * Get menu items by id
     */
    @GetMapping(API_BILL_ITEMS_URL + "/{"+ID+"}")
    @ApiOperation(value = "Get a bill item by id; return a bill with http status.")
    public ResponseEntity<BillItemDTO> getBillItemsById(@PathVariable int id) {
        return new ResponseEntity<>(billItemService.findById(id), HttpStatus.OK);
    }

    /**
     * Add menu items
     */
    @PostMapping(API_BILL_ITEMS_URL)
    @ApiOperation(value = "Create a bill item and return a new bill item with http status.")
    public ResponseEntity<BillItemDTO> createBillItem(@RequestBody BillItemDTO billItemDTO) {
        BillItemDTO resultBillItemDTO = billItemService.createOrUpdate(billItemDTO, 0, true);
        return new ResponseEntity<>(resultBillItemDTO, HttpStatus.OK);
    }

    /**
     * Update bill menu
     */
    @PutMapping(API_BILL_ITEMS_URL + "/{"+ID+"}")
    @ApiOperation(value = "Update an existing bill item then return a updated bill item and http status")
    public  ResponseEntity<BillItemDTO> updateBillItem(@PathVariable int id, @RequestBody BillItemDTO billItemDTO) {
        BillItemDTO resultBillItemDTO = billItemService.createOrUpdate(billItemDTO, id,false);
        return new ResponseEntity<>(resultBillItemDTO, HttpStatus.OK);
    }

    /**
     * Delete bill menu by id
     */
    @DeleteMapping(API_BILL_ITEMS_URL + "/{"+ID+"}")
    @ApiOperation(value = "Delete a bill item by id.")
    public ResponseEntity<Void> deleteBillItem(@PathVariable int id) {
        billItemService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

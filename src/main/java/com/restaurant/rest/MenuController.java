package com.restaurant.rest;


import com.restaurant.dto.MenuDTO;
import com.restaurant.service.GenericService;
import com.restaurant.utils.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Menu rest controller
 */
@RestController
@RequestMapping(Constant.menuUrl)
public class MenuController {
    //private static final String API_MENU_URL = "/api/menu";
    private static final String ID = "id";
    @Autowired
    private GenericService<MenuDTO> menuService;

    /**
     * Find all menu with sortBy(id/order date/ total_price)
     * page: ('size' objects / page)
     */
    @GetMapping
    @ApiOperation(value = "Get all menu with options: sortBy(ASC), page - default = 0, size - default = 5")
    public ResponseEntity<Page<MenuDTO>> getAllMenus(@RequestParam Optional<String> sortBy,
                                                     @RequestParam Optional<Integer> page,
                                                     @RequestParam Optional<Integer> size) {
        return new ResponseEntity<>(menuService.findAll(page, sortBy, size), HttpStatus.OK);
    }

    /**
     * Get menu by id
     */
    @GetMapping("/{"+ID+"}")
    @ApiOperation(value = "Get menu by id, return a menu with given id and the http status code.")
    public ResponseEntity<MenuDTO> getMenuById(@PathVariable int id) {
        return new ResponseEntity<>(menuService.findById(id), HttpStatus.OK);
    }

    /**
     * Add menu to the db
     */
    @PostMapping
    @ApiOperation(value = "Create a new menu to the database and return a new menu with http status")
    public ResponseEntity<MenuDTO> createMenu(@RequestBody MenuDTO menuDTO) {
        MenuDTO resultMenuDTO = menuService.createOrUpdate(menuDTO, 0, true);
        return new ResponseEntity<>(resultMenuDTO, HttpStatus.OK);
    }

    /**
     * Update menu
     */
    @PutMapping("/{"+ID+"}")
    @ApiOperation(value = "Update a menu by id and return a updated menu with http status")
    public ResponseEntity<MenuDTO> updateMenu(@PathVariable int id, @RequestBody MenuDTO menuDTO) {
        MenuDTO resultMenuDTO = menuService.createOrUpdate(menuDTO, id, false);
        return new ResponseEntity<>(resultMenuDTO, HttpStatus.OK);
    }

    /**
     * Delete menu by id
     */
    @DeleteMapping("/{"+ID+"}" )
    @ApiOperation(value = "Delete a menu by id")
    public ResponseEntity<Void> deleteMenu(@PathVariable int id) {
        menuService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

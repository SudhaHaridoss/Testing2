package com.restaurant.repository;

import com.restaurant.entity.BillItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
/**
 * Bill items repository
 */
@Repository
public interface BillItemRepository extends JpaRepository<BillItem, Integer> {
    @Query(value = "SELECT * FROM bill_items t WHERE t.menu_id = ?1",
            nativeQuery = true
    )
    List<BillItem> findAllByMenu_Id(int menuId);

}

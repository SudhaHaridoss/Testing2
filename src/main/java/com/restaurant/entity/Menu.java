package com.restaurant.entity;

import com.restaurant.utils.Constant;
import com.restaurant.utils.MenuType;

import javax.persistence.*;
import java.util.Set;

/**
 * Menu entity
 */
@Entity
@Table(name = Constant.ENTITY_MENU)
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Constant.ENTITY_MENU_ID)
    private int id;
    @Enumerated(EnumType.STRING)
    @Column(name = Constant.ENTITY_MENU_MENU_TYPE)
    private MenuType type;
    @Column(name = Constant.ENTITY_MENU_NAME)
    private String name;
    @Column(name = Constant.ENTITY_MENU_DESCRIPTION)
    private String description;
    @Column(name = Constant.ENTITY_MENU_PRICE)
    private double price;
    @OneToMany(mappedBy = "menu", cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    Set<BillItem> billItems;

    /**
     * Constructor with args and no-args
     */
    public Menu() {
    }

    /**
     * Contructor with args
     *
     * @param id
     * @param type
     * @param name
     * @param description
     * @param price
     */
    public Menu(int id, MenuType type, String name, String description, double price) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    /**
     * Get menu type
     *
     * @return
     */
    public MenuType getType() {
        return type;
    }

    /**
     * Set menu type
     *
     * @param menuType
     */
    public void setType(MenuType menuType) {
        this.type = menuType;
    }

    /**
     * Get id
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * set id
     *
     * @return
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * get Food Or Drink Name
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * set Food Or Drink Name
     *
     * @return
     */
    public void setName(String foodOrDrinkName) {
        this.name = foodOrDrinkName;
    }

    /**
     * get Description
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * set Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * get price
     *
     * @return
     */
    public double getPrice() {
        return price;
    }

    /**
     * set price
     */
    public void setPrice(double price) {
        this.price = price;
    }

}

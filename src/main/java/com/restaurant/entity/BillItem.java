package com.restaurant.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.restaurant.utils.Constant;

import javax.persistence.*;

/**
 * Bill menu entity to show
 * the relationship between bill and menu
 */
@Entity
@Table(name = Constant.ENTITY_BILL_ITEMS)
public class BillItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Constant.ENTITY_BILL_ITEMS_ID)
    private int id;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = Constant.ENTITY_BILL_ITEMS_BILL_ID)
    @JsonBackReference
    private Bill bill;
    @ManyToOne
    @JoinColumn(name = Constant.ENTITY_BILL_ITEMS_MENU_ID)
    private Menu menu;
    @Column(name = Constant.ENTITY_BILL_ITEMS_QUANTITY)
    private int quantity;

    /**
     * Constructor with args
     */
    public BillItem(Bill bill, Menu menu, int quantity) {
        this.bill = bill;
        this.menu = menu;
        this.quantity = quantity;
    }

    /**
     * Constructor no arg
     */
    public BillItem() {

    }

    /**
     * Get id
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * set  id
     *
     * @return
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get bill (id, total_price, ...)
     *
     * @return
     */
    public Bill getBill() {
        return bill;
    }

    /**
     * Set bill
     *
     * @param bill1
     */
    public void setBill(Bill bill1) {
        this.bill = bill1;
    }

    /**
     * Get menu
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * Get menu
     *
     * @param menu1
     */
    public void setMenu(Menu menu1) {
        this.menu = menu1;
    }


    /**
     * Get quantity
     *
     * @return
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Set quantity
     *
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

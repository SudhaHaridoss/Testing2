package com.restaurant.utils;

/**
 * Class to store urls
 */
public class Constant {
    public static final String billUrl = "/api/bill";
    // /api/bill/bill_item
    public static final String billItemsUrl = "/billitem";
    public static final String menuUrl = "/api/menu";

    public static final Integer DEFAULT_PAGE_NUMBER = 0;
    public static final Integer DEFAULT_PAGE_SIZE = 5;
    // BILL
    public static final String ENTITY_BILL = "bill";
    public static final String ENTITY_BILL_ID = "id";
    public static final String ENTITY_BILL_ORDER_DATE = "order_date";
    public static final String ENTITY_BILL_TOTAL_PRICE = "total_price";
    // BILL ITEM
    public static final String ENTITY_BILL_ITEMS = "bill_items";
    public static final String ENTITY_BILL_ITEMS_ID = "id";
    public static final String ENTITY_BILL_ITEMS_BILL_ID = "bill_id";
    public static final String ENTITY_BILL_ITEMS_MENU_ID = "menu_id";
    public static final String ENTITY_BILL_ITEMS_QUANTITY = "quantity";
    // MENU
    public static final String ENTITY_MENU = "menu";
    public static final String ENTITY_MENU_ID = "id";
    public static final String ENTITY_MENU_MENU_TYPE = "menu_type";
    public static final String ENTITY_MENU_NAME = "food_drink_name";
    public static final String ENTITY_MENU_DESCRIPTION = "description";
    public static final String ENTITY_MENU_PRICE = "price";

}

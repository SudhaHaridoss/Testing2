package com.restaurant.dto;

import com.restaurant.entity.Menu;

/**
 * Bill items DTO class
 */
public class BillItemDTO {
    private int id;
    private int billId;
    private Menu menu;
    private int quantity;

    /**
     * Constructor no arg
     */
    public BillItemDTO() {
    }

    /**
     * Constructor with args
     * @param id
     * @param billId
     * @param menu
     * @param quantity
     */
    public BillItemDTO(int id, int billId, Menu menu, int quantity) {
        this.id = id;
        this.billId = billId;
        this.menu = menu;
        this.quantity = quantity;

    }

    /**
     * Get id
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Set id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get bill
     * @return
     */
    public int getBillId() {
        return billId;
    }

    /**
     * Set bill
     * @param billId
     */
    public void setBillId(int billId) {
        this.billId = billId;
    }

    /**
     * Get menu
     * @return
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * Set menu
     * @param menu
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /**
     * Get quantity
     * @return
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Set quantity
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}

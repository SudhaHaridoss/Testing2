package com.restaurant.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;

/**
 * Bill DTO class
 */
public class BillDTO {
    private int id;
    private String date;
    private double total;
    // Use this annotation to make jackson dont show this in postman
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Map<Integer, Integer> menuAndQuantity;
    private Set<BillItemDTO> billItemDTOS;

    /**
     * Constructor no arg
     */
    public BillDTO() {
    }

    /**
     * Constructor with args for BillDTO
     * @param id
     * @param date
     * @param total
     */
    public BillDTO(int id, String date, double total) {
        this.id = id;
        this.date = date;
        this.total = total;
    }

    /**
     * Get date
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     * Set date
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Set date with input type = sql.Date
     * @param date
     */
    public void setDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String strDate = formatter.format(date);
        this.date = strDate;
    }

    /**
     * Get total
     * @return
     */
    public double getTotal() {
        return total;
    }

    /**
     * Set total
     * @param total
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * Get id
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Set id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * get bill items dto
     * @return
     */
    public Set<BillItemDTO> getBillItemDTOS() {
        return billItemDTOS;
    }

    /**
     * Set bill items dto
     * @param billItemDTOS
     */
    public void setBillItemDTOS(Set<BillItemDTO> billItemDTOS) {
        this.billItemDTOS = billItemDTOS;
    }

    /**
     * Get map of menu and quantity
     * @return
     */
//    @JsonIgnore
    public Map<Integer, Integer> getMenuAndQuantity() {
        return menuAndQuantity;
    }

    /**
     * Set map of menu and quanty
     * @param menuAndQuantity
     */
    public void setMenuAndQuantity(Map<Integer, Integer> menuAndQuantity) {
        this.menuAndQuantity = menuAndQuantity;
    }

    /**
     * To String
     * @return
     */
    @Override
    public String toString() {
        return "BillDTO{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", total=" + total +
                ", menuAndQuantity=" + menuAndQuantity +
                ", billItemDTOS=" + billItemDTOS +
                '}';
    }
}

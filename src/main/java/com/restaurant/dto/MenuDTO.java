package com.restaurant.dto;

import com.restaurant.utils.MenuType;

/**
 * Menu DTO Class
 */
public class MenuDTO {
    private int id;
    private String description;
    private String name;
    private double price;
    private MenuType type;

    /**
     * Constructor with no arg
     */
    public MenuDTO() {
    }

    /**
     * Constructor with args
     * @param id
     * @param description
     * @param name
     * @param price
     * @param type
     */
    public MenuDTO(int id, String description, String name, double price, MenuType type) {
        this.id = id;
        this.description = description;
        this.name = name;
        this.price = price;
        this.type = type;
    }

    /**
     * get id
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Set id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * get description
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set description
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * set name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * get price
     * @return
     */
    public double getPrice() {
        return price;
    }

    /**
     * set price
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * get type
     * @return
     */
    public MenuType getType() {
        return type;
    }

    /**
     * set type
     * @param type
     */
    public void setType(MenuType type) {
        this.type = type;
    }
}

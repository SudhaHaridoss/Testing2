package com.restaurant.service;

import com.restaurant.dto.MenuDTO;
/**
 * Menu service extended from GenericService => Easy to add new method specifically use for menu
 */
public interface MenuService extends GenericService<MenuDTO> {
}

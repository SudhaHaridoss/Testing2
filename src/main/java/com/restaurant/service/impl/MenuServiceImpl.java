package com.restaurant.service.impl;

import com.restaurant.dto.MenuDTO;
import com.restaurant.entity.Bill;
import com.restaurant.entity.BillItem;
import com.restaurant.entity.Menu;
import com.restaurant.exception.ExceptionHandler;
import com.restaurant.mapper.MenuMapper;
import com.restaurant.repository.BillItemRepository;
import com.restaurant.repository.BillRepository;
import com.restaurant.repository.MenuRepository;
import com.restaurant.service.MenuService;
import com.restaurant.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Implement class of GenericService for menu
 */
@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private BillItemRepository billItemRepository;
    @Autowired
    private BillRepository billRepository;
    @Autowired
    private MenuMapper menuMapper;
    private static final String ID = "id";
    private static final String EXCEPTION_MESSAGE_MENU = "Menu not found with id: ";

    /**
     * Find bill by id
     */
    @Override
    public MenuDTO findById(int id) {
        Optional<Menu> resultMenu = menuRepository.findById(id);
        Menu menu;
        if (resultMenu.isPresent()) {
            menu = resultMenu.get();
            // RETURN MENU ENTITY TO DTO
            return menuMapper.entityToDTO(menu);
        }
        throw new ExceptionHandler(EXCEPTION_MESSAGE_MENU + id);
    }

    /**
     * create (isCreate) or update(!isCreate) bill
     */
    @Override
    @Transactional
    public MenuDTO createOrUpdate(MenuDTO menuDTO, int paramId, boolean isCreate) {
        // Convert DTO to ENTITY
        Menu menu = menuMapper.dtoToEntity(menuDTO);
        // check if is updated
        if (!isCreate) {
            Optional<Menu> checkMenu = menuRepository.findById(menu.getId());
            if (checkMenu.isEmpty()) {
                throw new ExceptionHandler(EXCEPTION_MESSAGE_MENU + menu.getId());
            }
            List<BillItem> billItemList = billItemRepository.findAllByMenu_Id(menu.getId());
            // Update new bill item, and set new total for bill.
            for (BillItem billItem : billItemList) {
                double newItemPrice = menu.getPrice();
                double itemQuantity = billItem.getQuantity();
                double oldSubtotal = billItem.getMenu().getPrice() * itemQuantity;
                double newSubTotal = newItemPrice * itemQuantity;
                Bill newBill = billItem.getBill();
                newBill.setTotalPrice(newBill.getTotalPrice() - oldSubtotal + newSubTotal);
                billRepository.save(newBill);
                billItem.setBill(newBill);
                billItemRepository.save(billItem);
            }
        } else {
            menu.setId(0);
            Optional<Menu> checkMenu = menuRepository.findMenuByName(menu.getName());
            if (checkMenu.isPresent()) {
                throw new ExceptionHandler("Menu with name " + menu.getName() + " already exist in the database!");
            }
        }
        menuRepository.save(menu);
        return menuMapper.entityToDTO(menu);
    }

    /**
     * Delete by id
     */
    @Override
    @Transactional
    public void deleteById(int id) {
        Optional<Menu> menu = menuRepository.findById(id);

        if (menu.isEmpty()) {
            throw new ExceptionHandler(EXCEPTION_MESSAGE_MENU + id);
        }
        // Find relative bill items and bill
        List<BillItem> billItemList = billItemRepository.findAllByMenu_Id(id);
        for (BillItem billItem : billItemList) {
            Bill bill = billItem.getBill();
            bill.setTotalPrice(bill.getTotalPrice() - billItem.getMenu().getPrice() * billItem.getQuantity());
            if (bill.getTotalPrice() < 0) {
                billRepository.delete(bill);
            } else {
                billRepository.save(bill);
            }
            billItem.setBill(bill);
        }
        billItemRepository.deleteAll(billItemList);
        menuRepository.deleteById(id);

    }

    /**
     * get all bill
     * page: 'size' objects / page
     *
     * @param page sortBy(id, ...)
     */
    @Override
    public Page<MenuDTO> findAll(Optional<Integer> page, Optional<String> sortBy, Optional<Integer> size) {
        // CONVERT PAGE ENTITY TO PAGE DTO
        return menuMapper.entityToDTO(menuRepository.findAll(
                PageRequest.of(
                        page.orElse(Constant.DEFAULT_PAGE_NUMBER),
                        size.orElse(Constant.DEFAULT_PAGE_SIZE),
                        Sort.Direction.ASC, sortBy.orElse(ID)
                )
        ));
    }

    /**
     * get bill by id
     */
    @Override
    public List<MenuDTO> findAll() {
        // CONVERT LIST MENU ENTITY TO LIST DTO
        return menuMapper.entityToDTO(menuRepository.findAll());
    }
}

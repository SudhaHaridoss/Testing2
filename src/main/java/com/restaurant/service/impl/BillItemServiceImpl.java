package com.restaurant.service.impl;

import com.restaurant.dto.BillItemDTO;
import com.restaurant.entity.Bill;
import com.restaurant.entity.BillItem;
import com.restaurant.entity.Menu;
import com.restaurant.exception.ExceptionHandler;
import com.restaurant.mapper.BillItemMapper;
import com.restaurant.repository.BillItemRepository;
import com.restaurant.repository.BillRepository;
import com.restaurant.service.BillItemService;
import com.restaurant.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Implement class of GenericService for bill items
 */
@Service
public class BillItemServiceImpl implements BillItemService {

    @Autowired
    private BillItemRepository billItemRepository;
    @Autowired
    private BillRepository billRepository;
    @Autowired
    private BillItemMapper billItemMapper;
    private static final String ID = "id";

    /**
     * Find bill by id
     */
    @Override
    public BillItemDTO findById(int id) {
        Optional<BillItem> result = billItemRepository.findById(id);
        BillItem billItem;

        if (result.isPresent()) {
            billItem = result.get();
            // RETURN DTO USING CONVERTER
            return billItemMapper.entityToDTO(billItem);
        } else {
            throw new ExceptionHandler("Bill items not found with id :" + id);
        }
    }

    /**
     * create bill(isCreate) or update (!isCreate).
     */
    @Override
    @Transactional
    public BillItemDTO createOrUpdate(BillItemDTO billItemDTO, int paramId, boolean isCreate) {
        // GET BILL ITEMS ENTITY USING CONVERTER
        // GET BILL AND MENU ID FROM BILLITEM DTO and USE BILL AND MENU REPO
        // TO SET BILL AND MENU VALUE FOR BILLITEM
        BillItem billItem = billItemMapper.dtoToEntity(billItemDTO);
        int billItemDTO_billid = billItemDTO.getBillId();
        Bill setBill = billRepository.getById(billItemDTO_billid);
        billItem.setBill(setBill);
        // check if is updated
        if (!isCreate) {
            int billItemsId = billItem.getId();
            // check if match the param
            if (billItemsId != paramId) {
                throw new ExceptionHandler("Bill items with id " + paramId + " dont match the bill id in bill items " + billItemsId);
            }
            Optional<BillItem> checkBillItems = billItemRepository.findById(billItemsId);
            double oldPrice;
            double newPrice;
            // check if exist
            if (checkBillItems.isPresent()) {
                oldPrice = checkBillItems.get().getMenu().getPrice() * checkBillItems.get().getQuantity();
                newPrice = billItem.getQuantity() * billItem.getMenu().getPrice();
                Optional<Bill> bill = billRepository.findById(billItem.getBill().getId());
                if (bill.isPresent()) {
                    // SET NEW TOTAL PRICE FOR BILL
                    bill.get().setTotalPrice(bill.get().getTotalPrice() - oldPrice + newPrice);
                    billRepository.save(bill.get());
                    // SET NEW BILL FOR BILL ITEM
                    billItem.setBill(bill.get());
                }
            } else {
                throw new ExceptionHandler("Bill items not found with id: " + billItemsId);
            }
            // Is add new
        } else {
            billItem.setId(0);
            int billId = billItem.getBill().getId();
            Menu menu = billItem.getMenu();

            int quantity = billItem.getQuantity();
            double itemPrice = menu.getPrice();

            double subTotal = itemPrice * quantity;

            // set the total price for bill and order date.
            Optional<Bill> bill = billRepository.findById(billId);
            if (bill.isPresent()) {
                bill.get().setTotalPrice(bill.get().getTotalPrice() + subTotal);
                bill.get().setOrderDate(String.valueOf(LocalDateTime.now()));
                billRepository.save(bill.get());
                // set the latest update bill to bill items
                billItem.setBill(bill.get());
                billItemRepository.save(billItem);

            } else {
                throw new ExceptionHandler("Bill not found with id: " + billId);
            }
        }
        billItemRepository.save(billItem);
        // Convert back to DTO and return
        return billItemMapper.entityToDTO(billItem);
    }

    /**
     * Delete by id
     */
    @Override
    @Transactional
    public void deleteById(int id) {
        Optional<BillItem> billItems = billItemRepository.findById(id);
        double menuItemPrice;
        int billId;
        if (billItems.isPresent()) {
            menuItemPrice = billItems.get().getMenu().getPrice() * billItems.get().getQuantity();
            billId = billItems.get().getBill().getId();
        } else {
            throw new ExceptionHandler("Bill items not found with id: " + id);
        }
        Optional<Bill> newBillAfterDeleteBillItems = billRepository.findById(billId);
        if (newBillAfterDeleteBillItems.isPresent()) {
            newBillAfterDeleteBillItems.get().setTotalPrice(newBillAfterDeleteBillItems.get().getTotalPrice() - menuItemPrice);
            if (newBillAfterDeleteBillItems.get().getTotalPrice() < 0) {
                billRepository.delete(newBillAfterDeleteBillItems.get());
            }
            billRepository.save(newBillAfterDeleteBillItems.get());
        } else {
            throw new ExceptionHandler("Bill not found with id: " + billId);
        }
        billItemRepository.deleteById(id);

    }

    /**
     * get all bill
     * page: 5 objects / page
     *
     * @param page sortBy(id, ...)
     */
    @Override
    public Page<BillItemDTO> findAll(Optional<Integer> page, Optional<String> sortBy, Optional<Integer> size) {
        // CONVERT  PAGE BILL ITEMS ENTITIES TO  PAGE DTO
        return billItemMapper.entityToDTO(billItemRepository.findAll(
                PageRequest.of(
                        page.orElse(Constant.DEFAULT_PAGE_NUMBER),
                        size.orElse(Constant.DEFAULT_PAGE_SIZE),
                        Sort.Direction.ASC, sortBy.orElse(ID)
                )
        ));
    }

    /**
     * get bill by id
     */
    @Override
    public List<BillItemDTO> findAll() {
        // CONVERT LIST BILL ITEMS ENTITIES TO  LIST DTO
        return billItemMapper.entityToDTO(billItemRepository.findAll());
    }
}

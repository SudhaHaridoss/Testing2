package com.restaurant.service.impl;

import com.restaurant.dto.BillDTO;
import com.restaurant.dto.BillItemDTO;
import com.restaurant.entity.Bill;
import com.restaurant.entity.Menu;
import com.restaurant.exception.ExceptionHandler;
import com.restaurant.mapper.BillMapper;
import com.restaurant.repository.BillRepository;
import com.restaurant.repository.MenuRepository;
import com.restaurant.service.BillItemService;
import com.restaurant.service.BillService;
import com.restaurant.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Implement class of GenericService for bill
 */
@Service
public class BillServiceImpl implements BillService {
    @Autowired
    private BillRepository billRepository;
    @Autowired
    private BillItemService billItemService;
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private BillMapper billMapper;
    private static final String ID = "id";


    /**
     * Find bill by id
     */
    @Override
    public BillDTO findById(int id) {
        Optional<Bill> resultBill = billRepository.findById(id);

        Bill bill;
        if (resultBill.isPresent()) {
            bill = resultBill.get();
            // CONVERT BILL ENTITY TO DTO
            return billMapper.entityToDTO(bill);
        } else {
            throw new ExceptionHandler("Bill not found with id: " + id);
        }
    }

    /**
     * save bill
     */

    @Override
    @Transactional
    public BillDTO createOrUpdate(BillDTO billDTO, int paramId, boolean isCreate) {
        // CONVERT BILL DTO TO BILL THEN SAVE
        Bill bill = billMapper.dtoToEntity(billDTO);
        if (bill == null) {
            throw new ExceptionHandler("Bill not found");
        }

        if (isCreate) {
            bill.setTotalPrice(0.0);
            bill.setBillItems(new HashSet<>());
            bill.setId(0);
        } else {
            if (bill.getId() != paramId) {
                throw new ExceptionHandler("Bill with id " + bill.getId() + " not math the param id " + paramId);
            }
            Bill checkBill = billRepository.getById(paramId);
            if (checkBill.getTotalPrice() != bill.getTotalPrice()){
                throw new ExceptionHandler("Bill with id "+ bill.getId() +": total price cannot be changed ("
                        +bill.getTotalPrice() +" should be "+checkBill.getTotalPrice()+" )");
            }
        }
        billRepository.save(bill);
        return billMapper.entityToDTO(bill);
    }


    /**
     * Delete by id
     */
    @Override
    @Transactional
    public void deleteById(int id) {
        Optional<Bill> bill = billRepository.findById(id);
        if (bill.isPresent()) {
            billRepository.deleteById(id);
        } else {
            throw new ExceptionHandler("Bill not found with id: " + id);
        }
    }

    /**
     * get all bill
     * page: 5 objects / page
     *
     * @param page sortBy(id, ...)
     */
    @Override
    public Page<BillDTO> findAll(Optional<Integer> page, Optional<String> sortBy, Optional<Integer> size) {
        // CONVERT PAGE BILL TO PAGE BILL DTO
        return billMapper.entityToDTO(billRepository.findAll(
                PageRequest.of(
                        page.orElse(Constant.DEFAULT_PAGE_NUMBER),
                        size.orElse(Constant.DEFAULT_PAGE_SIZE),
                        Sort.Direction.ASC, sortBy.orElse(ID)
                )
        ));
    }

    /**
     * get bill by id
     * this is used to make junit test case
     */
    @Override
    public List<BillDTO> findAll() {
        // CONVERT LIST BILL TO LIST DTO
        return billMapper.entityToDTO(billRepository.findAll());
    }


    /**
     * Create new bill items directly through bill
     * by list of bill items
     *
     * @param billDTO
     * @return
     */
    @Override
    @Transactional
    public BillDTO createListBillItems(BillDTO billDTO, int paramId) {
        // DOESN'T HAVE TOTAL AND DATE AND BILL ITEMS LIST
        // FIND BILL WITH BILL ID OF billDTO
        Optional<Bill> checkBill = billRepository.findById(billDTO.getId());
        // CHECK IF id in billDTO match the paramId
        if (billDTO.getId() != paramId) {
            throw new ExceptionHandler("Bill with id " + billDTO.getId() + " not match the param id");
        }
        // CHECK IF Bill don't exist with this id.
        if (checkBill.isEmpty()) {
            throw new ExceptionHandler("Bill with id " + billDTO.getId() + " not found");
        }
        // GET menuAndQuantity from billDTO
        Map<Integer, Integer> menuAndQuantity = billDTO.getMenuAndQuantity();
        // LOOP A MAP <Menu ID, Quantity>
        Iterator<Map.Entry<Integer, Integer>> iterator = menuAndQuantity.entrySet().iterator();
        // RESULT LIST
        List<BillItemDTO> resultBillItemList = new ArrayList<>();
        while (iterator.hasNext()) {
            // THIS INCLUDE menu id
            Map.Entry<Integer, Integer> entry = iterator.next();
            // GET MENU BY MENU ID AND ADD IT TO BILL ITEM
            Optional<Menu> billItemMenu = menuRepository.findById(entry.getKey());
            if (billItemMenu.isEmpty()) {
                throw new ExceptionHandler("Menu not found with id " + entry.getKey());
            }
            BillItemDTO billItemDTO = new BillItemDTO(0, billDTO.getId(), billItemMenu.get(), entry.getValue());
            billItemDTO = billItemService.createOrUpdate(billItemDTO, 0, true);
            resultBillItemList.add(billItemDTO);
        }
        // DECLARE NEW TOTAL VARIABLE TO STORE NEW PRICE AFTER ADD A LIST OF billItemDTO
        double newTotal = billRepository.getById(paramId).getTotalPrice();
        Set<BillItemDTO> resultBillItemSet = new HashSet<>(resultBillItemList);
        Bill newBillAfterAdd = billRepository.getById(paramId);
        // SET NEW DATE FOR billDTO and new List of Bill items
        billDTO.setDate(newBillAfterAdd.getOrderDate());
        billDTO.setBillItemDTOS(resultBillItemSet);
        billDTO.setTotal(newTotal);
        return billDTO;
    }
}

package com.restaurant.service;


import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 * Generic service class
 *
 * @param <T>
 */
public interface GenericService<T> extends ServiceInterface {


    /**
     * Find bill by id
     */
    T findById(int id);

    /**
     * save bill
     *
     * @param
     */

    T createOrUpdate(T t, int id, boolean isCreate);

    /**
     * Delete by id
     */
    void deleteById(int id);

    /**
     * get all bill
     * page: 5 objects / page
     *
     * @param page sortBy(id, ...)
     * @return
     */
    Page<T> findAll(Optional<Integer> page, Optional<String> sortBy, Optional<Integer> size);

    /**
     * get bill by id
     */
    List<T> findAll();
}

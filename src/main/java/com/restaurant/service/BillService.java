package com.restaurant.service;

import com.restaurant.dto.BillDTO;
/**
 * Bill service extended from GenericService => Easy to add new method specifically use for bill
 */
public interface BillService extends GenericService<BillDTO>{
    /**
     * Create new bill items directly through bill
     * @param billDTO
     * @return
     */
    BillDTO createListBillItems(BillDTO billDTO, int id);
}

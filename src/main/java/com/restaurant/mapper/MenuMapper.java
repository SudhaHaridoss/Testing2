package com.restaurant.mapper;

import com.restaurant.dto.MenuDTO;
import com.restaurant.entity.Menu;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper class for menu
 */
@Component
public class MenuMapper implements GenericMapper<MenuDTO, Menu>{
    /**
     * Convert menu entity to MenuDTO
     *
     * @param menu
     * @return
     */
    @Override
    public MenuDTO entityToDTO(Menu menu) {
        MenuDTO menuDTO = new MenuDTO();
        menuDTO.setName(menu.getName());
        menuDTO.setId(menu.getId());
        menuDTO.setDescription(menu.getDescription());
        menuDTO.setType(menu.getType());
        menuDTO.setPrice(menu.getPrice());
        return menuDTO;
    }

    /**
     * Convert List Menu to list Menu dto
     *
     * @param menuList
     * @return
     */
    @Override
    public List<MenuDTO> entityToDTO(List<Menu> menuList) {
        return menuList.stream().map(
                        this::entityToDTO)
                .collect(Collectors.toList());
    }

    /**
     * Convert Set of Menu to set of menuDTO
     * @param menuSet
     * @return
     */
    @Override
    public Set<MenuDTO> entityToDTO(Set<Menu> menuSet) {
        return menuSet.stream().map(
                        this::entityToDTO)
                .collect(Collectors.toSet());
    }

    /**
     * Convert Page menu to Page menuDTO
     * @param menuPage
     * @return
     */
    @Override
    public Page<MenuDTO> entityToDTO(Page<Menu> menuPage) {
        List<Menu> menuList = menuPage.getContent();
        List<MenuDTO> menuDTOList = entityToDTO(menuList);
        return new PageImpl<>(menuDTOList, menuPage.getPageable(), menuDTOList.size());
    }

    /**
     * Convert DTO to Menu Entity
     *
     * @param menuDTO
     * @return
     */
    @Override
    public Menu dtoToEntity(MenuDTO menuDTO) {
        Menu menu = new Menu();
        menu.setId(menuDTO.getId());
        menu.setName(menuDTO.getName());
        menu.setDescription(menuDTO.getDescription());
        menu.setType(menuDTO.getType());
        menu.setPrice(menuDTO.getPrice());
        return menu;
    }

    /**
     * Convert List DTOs to List Menu entities
     *
     * @param menuDTOList
     * @return
     */
    @Override
    public List<Menu> dtoToEntity(List<MenuDTO> menuDTOList) {
        return menuDTOList.stream().map(
                        this::dtoToEntity)
                .collect(Collectors.toList());
    }


}

package com.restaurant.mapper;

import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Set;

/**
 * Generic Mapper for Bill, Bill item, menu
 * @param <T>
 * @param <U>
 */
public interface GenericMapper<T, U> {
    T entityToDTO(U u);
    List<T> entityToDTO(List<U> uList);
    Set<T> entityToDTO(Set<U> uSet);
    Page<T> entityToDTO(Page<U> uPage);

    U dtoToEntity(T t);
    List<U> dtoToEntity(List<T> tList);


}

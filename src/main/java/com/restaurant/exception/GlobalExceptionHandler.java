package com.restaurant.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Global exception handler for bill
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    /**
     * Exception handler for not found
     * Same menu name
     * @param exc
     * @return
     */
    @ExceptionHandler
    public ResponseEntity<ErrorDetails> handleException(com.restaurant.exception.ExceptionHandler exc) {
        ErrorDetails error = new ErrorDetails();

        error.setStatus(HttpStatus.NOT_FOUND.value());
        error.setMessage(exc.getMessage());

        // return R.E
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
    /**
     * Exception handler for wrong syntax (expect int but receive String).
     * @param exc
     * @return
     */
    @ExceptionHandler
    public ResponseEntity<ErrorDetails> handleException(Exception exc) {
        exc.printStackTrace();
        ErrorDetails error = new ErrorDetails();
        if (exc instanceof HttpMessageNotReadableException){
            error.setMessage("Error! MenuType should be [BREAKFAST, LUNCH, DINNER, SOFT, ALCOHOL], Food or Drink name should be String.");
        }else {
            error.setMessage(exc.getMessage());
        }
        error.setStatus(HttpStatus.BAD_REQUEST.value());

        // return R.E
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}

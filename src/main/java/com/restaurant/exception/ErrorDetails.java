package com.restaurant.exception;

/**
 * Class that contains details for error
 */
public class ErrorDetails {
    private int status;
    private String message;

    /**
     * constructor with no arg
     */
    public ErrorDetails() {
    }

    /**
     * Constructor with args
     * @param status
     * @param message
     */
    public ErrorDetails(int status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * get status
     * @return
     */
    public int getStatus() {
        return status;
    }

    /**
     * set status
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * get message
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     * set message
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}

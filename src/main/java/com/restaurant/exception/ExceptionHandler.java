package com.restaurant.exception;
/**
 * Exception for bill/bill items/ menu not found
 * Same menu name
 */
public class ExceptionHandler extends RuntimeException{
    public ExceptionHandler(String message) {
        super(message);
    }
}

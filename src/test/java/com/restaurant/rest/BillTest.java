package com.restaurant.rest;

import com.restaurant.mapper.BillMapper;
import com.restaurant.dto.BillDTO;
import com.restaurant.entity.Bill;
import com.restaurant.service.GenericService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BillTest {

    @Autowired
    GenericService<BillDTO> billService;
    @Autowired
    BillMapper billMapper;
    @Test
    void findAll() {
        assertEquals(5, billService.findAll().size());
    }

    @Test
    void getBill() {
        Bill bill = new Bill(2, "2021-08-17 00:00:00", 180000);
        assertEquals(bill, billService.findById(2));
    }



    @Test
    void updateBill() {
        Bill bill = new Bill(2, "2021-08-18", 180000);
        billService.createOrUpdate(billMapper.entityToDTO(bill), bill.getId(), false);
        assertEquals(bill.getOrderDate(), billService.findById(2).getDate());
    }


}
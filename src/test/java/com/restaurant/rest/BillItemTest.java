package com.restaurant.rest;

import com.restaurant.mapper.BillItemMapper;
import com.restaurant.dto.BillItemDTO;
import com.restaurant.entity.BillItem;
import com.restaurant.service.GenericService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BillItemTest {

    @Autowired
    GenericService<BillItemDTO> billMenuService;
    @Autowired
    BillItemMapper billItemMapper;
    @Test
    void findAll() {
        assertEquals(3, billMenuService.findAll().size());
    }

    @Test
    void getMenuItems() {
        BillItem billItem = billItemMapper.dtoToEntity(billMenuService.findById(2));
        assertEquals(2, billItem.getId());
    }



    @Test
    void updateMenuItems() {
        BillItem billItem = billItemMapper.dtoToEntity(billMenuService.findById(2));
        billItem.setQuantity(3);
        billMenuService.createOrUpdate(billItemMapper.entityToDTO(billItem), billItem.getBill().getId(), false);
        assertEquals(3, billMenuService.findById(2).getQuantity());
    }


}
package com.restaurant.rest;

import com.restaurant.mapper.MenuMapper;
import com.restaurant.dto.MenuDTO;
import com.restaurant.entity.Menu;
import com.restaurant.service.GenericService;
import com.restaurant.utils.MenuType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MenuTest {

    @Autowired
    GenericService<MenuDTO> menuService;
    @Autowired
    MenuMapper menuMapper;
    @Test
    void findAll() {
        assertEquals(49, menuService.findAll().size());
    }

    @Test
    void getMenu() {
        Menu menu = menuMapper.dtoToEntity(menuService.findById(2));
        assertEquals(2, menu.getId());
    }



    @Test
    void updateMenu() {
//        1,Voluptatibus in iste. Voluptas quibusdam odio; omnis cum architecto. Est natus aut. Voluptatibus corrupti at. Reiciendis non error. Qui sapiente minima.,agave,100000,ALCOHOL
                Menu temp = menuMapper.dtoToEntity(menuService.findById(1));
                Menu menu = new Menu();
                menu.setId(1);
                menu.setType(MenuType.ALCOHOL);
                menu.setPrice(10000);
                menu.setName("Vodka");
                menu.setDescription("Ruou ngon");
                menuService.createOrUpdate(menuMapper.entityToDTO(menu), menu.getId(), false);
                assertNotEquals(menu, temp);
    }


}
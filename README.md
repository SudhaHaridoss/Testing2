The app includes packages like entity, repository, rest, service, utils.
- Package entity contains 3 entities including Bill, Menu, MenuItems corresponding to the database.
- Repository Contains 3 repositories to interact with the database corresponding to 3 tables: bill, menu, menu_items
- Package Rest contains 2 controllers (BillRestController, MenuRestController). BillRestController to handle requests like Get, Post, Put, Delete for Bill and MenuItems in the database. MenuRestController to handle requests like Get, Post, Put, Delete for Menu.
- Class Service includes 1 GenericService interface and 3 classes that implement BillServiceImpl, MenuServiceImpl, MenuItemsServiceImpl.
- Utils to contain Enumerate MenuType including 5 types: Breakfast, Lunch, Dinner, Soft Drink, Alcohol Drink.
Class RestaurantProjectApplication to run Spring Boot.
Package resources: file application.properties contain settings including configs.
 
 
- Bill:
/api/bill/ to get list of bills, post bill, put, delete bill.
/bill?sortBy={fieldname} to sort by the name of the field(name, totalPrice, date) in ascending order.
/bill?page=page number to find a list of bills by page (0, 1, ...) with 5 bills per page.
/api/bill/{id} to search id by bill
- Menus:
 /api/menu/ to get list of menus, post menu, put, delete menu.
/menu?sortBy={fieldname} to sort by the name of the field(name, totalPrice, date) in ascending order.
/menu?page=page number to find a list of menus by page (0, 1, ...) with 5 bills per page.
/api/menu/{id} to search id by bill
- Bill Items:
/api/bill_item/ to get list of bill items, post,put ,delete
/api/bill_item/${id} to get bill items with id
/bill_item?sortBy={fieldname} to sort by the name of the field(name, totalPrice, date) in ascending order.
/bill_item?page=page number to find a list of bill items by page (0, 1, ...) with 5 bill items per page.

- To run the project: use mvn spring-boot:run to run the project using your computer maven, .\mvnw spring-boot:run
- To tes the project: use mvn test.
After running the app: use the path http://localhost:8080/swagger-ui.html#/ to view the Swagger UI

